# IT

## Descrizione esercizio

L'esercizio richiede la creazione di un'infrastruttura di produzione per l'applicazione mongo_dbs.js. 

L'applicazione usa un database MongoDB e necessita dei due package mongodb ed express. Inoltre necessita delle due variabili di ambiente :

- MONGO_URI="mongodb://username:password@hostname:port""
- HTTP_PORT="xxxx"

Una volta avviata si connette al database MongoDB e visualizza l'elenco dei database presenti. 

Chiamando l'applicazione con http://hostname_web:port/STOP se ne simula il crash.

Obiettivo dell'esercizio è:

- Automatizzare la creazione dell'infrastruttura
- Automatizzare il setup dell'applicazione.
- Implementare un metodo per il riavvio automatico del servizio in caso di crash
- Automatizzare il backup del database

L'ouptput dell'esercizio deve essere:

- il codice utilizzato per produrre l'automazione e la configurazione 
- un file (README.md) con una descrizione che consenta di comprendere il codice ed eseguirlo
- un disegno dell'architettura realizzato con Draw.io (https://app.diagrams.net/)

E' possibile usare una qualsiasi piattaforma public cloud ed un qualsiasi linguaggio di programmazione/automazione.

Il tempo per l'esecuzione dell'esercizio è di una settimana a partire dalla ricezione per email.

# EN

## Exercise Description

The exercise requires the creation of a production infrastructure for the mongo_dbs.js application.

The application uses a MongoDB database and needs the two packages mongodb and express. It also needs the two environment variables :

- MONGO_URI="mongodb://username:password@hostname:port""
- HTTP_PORT="xxxx"

Once started, it connects to the MongoDB database and displays the list of databases present.

Calling the application with http://hostname_web:port/STOP simulates a crash.

Objective of the exercise is:

- Automate the creation of the infrastructure
- Automate the application setup.
- Implement a method to automatically restart the service in case of a crash.
- Automate the database backup

The ouptput of the exercise must be:

- the code used to produce the automation and configuration
- a file (README.md) with a description that allows you to understand the code and execute it
- a drawing of the architecture created with Draw.io (https://app.diagrams.net/)

It is possible to use any public cloud platform and any programming/automation language.

The time for the execution of the exercise is one week starting from the receipt by email.

